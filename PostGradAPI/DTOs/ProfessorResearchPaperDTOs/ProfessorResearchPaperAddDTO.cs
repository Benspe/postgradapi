﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PostGradAPI.DTOs.ProfessorResearchPaperDTOs
{
    public class ProfessorResearchPaperAddDTO
    {
        [Required]
        public int ProfessorID { get; set; }
        [Required]
        public int ResearchPaperID { get; set; }

    }
}
