﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PostGradAPI.DTOs.ProfessorResearchPaperDTOs
{
    public class ProfessorResearchPaperReadDTO
    {
                
        [Required]
        [MaxLength(255)]
        public int ProfessorID { get; set; }
        [Required]
        [MaxLength(255)]
        public int ResearchPaperID { get; set; }

        [Required]
        [MaxLength(255)]
        public string Professor { get; set; }
        
        [Required]
        [MaxLength(255)]
        public string ResearchPaper { get; set; }
    
}
}
