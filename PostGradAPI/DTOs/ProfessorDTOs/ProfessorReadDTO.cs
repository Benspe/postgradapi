﻿using PostGradAPI.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PostGradAPI.DTOs.ProffessorDTOs
{
    public class ProfessorReadDTO
    {

        [Required]
        public int ID { get; set; }

        [Required]
        [MaxLength(255)]
        public string FullName { get; set; }

        [MaxLength(255)]
        public string Subject { get; set; }
    }
}
