﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PostGradAPI.DTOs.StudentDTOs
{
    public class StudentReadDTO
    {
        public int ID { get; set; }

        [Required]
        [MaxLength(255)]
        public string FullName { get; set; }
        public string Major { get; set; }
        public string Supervisor { get; set; }
        public int SupervisorID { get; set; }

    }
}
