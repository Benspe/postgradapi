﻿using PostGradAPI.DTOs.ProfessorResearchPaperDTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Xml;

namespace PostGradAPI.DTOs.ResearchPaperDTOs
{
    public class ResearchPaperReadDTO
    {
        public int ID { get; set; }
        public string Title { get; set; }
        
        public int[] AuthorIDs { get;set; }
    }
}
