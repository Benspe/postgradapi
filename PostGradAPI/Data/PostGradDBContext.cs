﻿using Microsoft.EntityFrameworkCore;
using PostGradAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PostGradAPI.Data
{
    public class PostGradDBContext : DbContext
    {
        public DbSet<Professor> Professors { get; set; }
        public DbSet<Student> Students { get; set; }

        public DbSet<ResearchPaper> ResearchPapers { get; set; }

        public DbSet<ProfessorResearchPaper> ProfessorResearchPapers { get; set; }

        public PostGradDBContext(DbContextOptions<PostGradDBContext> options): base(options)
        { 
        
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ProfessorResearchPaper>().HasKey(pq => new
            {
                pq.ProfessorID,
                pq.ResearchPaperID
            });
        }
    }
}
