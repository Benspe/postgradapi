﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PostGradAPI.Data;
using PostGradAPI.DTOs.ProfessorResearchPaperDTOs;
using PostGradAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PostGradAPI.Controllerse
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProfessorResearchPaperController : ControllerBase
    {

        private readonly PostGradDBContext _context;
        private readonly IMapper _mapper;
        public ProfessorResearchPaperController(PostGradDBContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        /// <summary>
        /// Gets all ProfessorResearchPaper from database using eagerloading.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult<IEnumerable<ProfessorResearchPaperReadDTO>> GetAllPRPs()
        {
            return _mapper.Map<List<ProfessorResearchPaperReadDTO>>(_context.ProfessorResearchPapers.Include(p =>p.Professor).Include(p=>p.ResearchPaper));
        }

        /// <summary>
        /// Adds a ProfessorResearchPaper in the database.
        /// </summary>
        /// <param name="prpIn"></param>
        /// <returns>The added entry as a ProfessorResearchPaperReadDTO</returns>
        [HttpPost]
        public async Task<ActionResult<ProfessorResearchPaperReadDTO>> CreatePRP(ProfessorResearchPaperAddDTO prpIn) 
        {
            var mappedPRP = _mapper.Map<ProfessorResearchPaper>(prpIn);
            await _context.AddAsync(mappedPRP);
            await _context.SaveChangesAsync();
            return _mapper.Map<ProfessorResearchPaperReadDTO>(mappedPRP);
        }
    }
}
