﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PostGradAPI.Data;
using PostGradAPI.DTOs.StudentDTOs;
using PostGradAPI.Models;

namespace PostGradAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StudentController : ControllerBase
    {
        private readonly PostGradDBContext _context;
        private readonly IMapper _mapper;
        public StudentController(PostGradDBContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        [HttpGet]
        public ActionResult<IEnumerable<StudentReadDTO>> GetAllStudents()
        {
            return _mapper.Map<List<StudentReadDTO>>(_context.Students.Include(s => s.Supervisor));
        }

        [HttpPost]
        public async Task< ActionResult<StudentReadDTO>> CreateStudent(StudentAddDTO studentAdd)
        {
            Student student = _mapper.Map<Student>(studentAdd);
            await _context.Students.AddAsync(student);
            _context.SaveChanges();
            return _mapper.Map<StudentReadDTO>(student);
        }

        [HttpPut("{id}")]
        public async Task< ActionResult> UpdateStudent(int id, StudentUpdateDTO studentUpdate)
        {
            Student student = _mapper.Map<Student>(studentUpdate);
            if (student.ID != id)
            {
                return BadRequest();
            }
            _context.Entry(student).State = EntityState.Modified;
            await _context.SaveChangesAsync();
            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult<StudentReadDTO>> DeleteStudent(int id, StudentDeleteDTO studentDelete) 
        {
            Student student = _mapper.Map<Student>(studentDelete);
            if (student.ID != id)
            {
                return BadRequest();
            }
            _context.Students.Remove(student);
            await _context.SaveChangesAsync();

            return _mapper.Map<StudentReadDTO>(student);

        }

    }
}
