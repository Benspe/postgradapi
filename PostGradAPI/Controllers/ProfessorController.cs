﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PostGradAPI.Data;
using PostGradAPI.DTOs.ProfessorDTOs;
using PostGradAPI.DTOs.ProffessorDTOs;
using PostGradAPI.Models;

namespace PostGradAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProfessorController : ControllerBase
    {
        private readonly PostGradDBContext _context;
        private readonly IMapper _mapper;
        public ProfessorController(PostGradDBContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }


        /// <summary>
        /// Deletes a Professor from database.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>A readable deleted professor</returns>
        [HttpDelete("{id}")]
        public async Task<ActionResult<ProfessorDeleteDTO>> DeleteProfessor(int id) 
        {
           var professor = await _context.Professors.FindAsync(id);

            if (professor == null) 
            {
                return NotFound();
            }
            _context.Professors.Remove(professor);
            await _context.SaveChangesAsync();
            return _mapper.Map<ProfessorDeleteDTO>(professor) ;
        }

        /// <summary>
        /// Updates a professor in the DB
        /// </summary>
        /// <param name="id"></param>
        /// <param name="professorUpdate"></param>
        /// <returns>The updated professor, BadRequest if id does not match professorUpdate.ID</returns>
        [HttpPut("{id}")]
        public async Task<ActionResult> UpdateProfessor(int id, ProfessorUpdateDTO professorUpdate) 
        {
            if (id != professorUpdate.ID) 
            { 
                return BadRequest();
            }
           var professor = _mapper.Map<Professor>(professorUpdate);
            _context.Entry(professor).State = EntityState.Modified;
            await _context.SaveChangesAsync();
            return NoContent();
        }

        /// <summary>
        /// Puts a professor entry in the database.
        /// </summary>
        /// <param name="professorAddDto"></param>
        /// <returns>A readable ProfessorReadDTO of the professorAdded.</returns>
        [HttpPost]
        public async Task<ActionResult<ProfessorReadDTO>> CreateProfessor(ProfessorAddDTO professorAddDto)
        {
            Professor professor =  _mapper.Map<Professor>(professorAddDto);

            await _context.Professors.AddAsync(professor);
            _context.SaveChanges();
            return _mapper.Map<ProfessorReadDTO>(professor);
        }

        /// <summary>
        /// Finds a professor by id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>The professor with the id</returns>
        [HttpGet("{id}")]
        public ActionResult<ProfessorReadDTO> GetProfessorByID(int id) 
        {
            var professor = _context.Professors.Find(id);
            var professorDto = _mapper.Map<ProfessorReadDTO>(professor);
            return professorDto;
        }

        /// <summary>
        /// Gets all professors from the database.
        /// </summary>
        /// <returns>Loaded Professors with eagerLoading</returns>
        [HttpGet]
        public ActionResult<IEnumerable<ProfessorReadDTO>> GetAllProfessors()
        {
            return _mapper.Map<List<ProfessorReadDTO>>(_context.Professors);
        }
    }
}
