﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PostGradAPI.Data;
using PostGradAPI.DTOs.ProfessorResearchPaperDTOs;
using PostGradAPI.DTOs.ResearchPaperDTOs;
using PostGradAPI.Models;

namespace PostGradAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ResearchPaperController : ControllerBase
    {

        private readonly PostGradDBContext _context;
        private readonly IMapper _mapper;
        public ResearchPaperController(PostGradDBContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }



        [HttpGet]
        public ActionResult<IEnumerable<ResearchPaperReadDTO>> GetAllResearchPapers()
        {
           return _mapper.Map<List<ResearchPaperReadDTO>>(_context.ResearchPapers.Include(p => p.ProfessorResearchPaper));
        }

        
        [HttpPost]
        public async Task<ActionResult<ResearchPaperReadDTO>> CreateResearchPaper(ResearchPaper researchPaper) 
        {
            await _context.ResearchPapers.AddAsync(researchPaper);
            await _context.SaveChangesAsync();
            return _mapper.Map<ResearchPaperReadDTO>(researchPaper);
        }


    }
}
