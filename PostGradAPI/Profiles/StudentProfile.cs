﻿using AutoMapper;
using PostGradAPI.DTOs.StudentDTOs;
using PostGradAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PostGradAPI.Profiles
{
    public class StudentProfile : Profile
    {
        public StudentProfile() 
        {
            var readMap = CreateMap<Student, StudentReadDTO>().
                ForMember(sRead => sRead.Supervisor, s => s.MapFrom(sup => sup.Supervisor.FullName));
            readMap.ReverseMap();

            var addMap = CreateMap<Student, StudentAddDTO>();
            addMap.ReverseMap();

            var updateMap = CreateMap<Student, StudentUpdateDTO>();
            updateMap.ReverseMap();

            var deleteMap = CreateMap<Student, StudentDeleteDTO>();
            deleteMap.ReverseMap();
        }
    }
}
