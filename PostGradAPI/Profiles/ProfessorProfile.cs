﻿using AutoMapper;
using PostGradAPI.DTOs.ProfessorDTOs;
using PostGradAPI.DTOs.ProffessorDTOs;
using PostGradAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PostGradAPI.Profiles
{
    public class ProfessorProfile : Profile
    {

        public ProfessorProfile()
        {
            var readMap = CreateMap<Professor, ProfessorReadDTO>();
            readMap.ReverseMap();

            var addMap = CreateMap<Professor, ProfessorAddDTO>();
            addMap.ReverseMap();

            var updateMap = CreateMap<Professor, ProfessorUpdateDTO>();
            updateMap.ReverseMap();

            var deleteMap = CreateMap<Professor, ProfessorDeleteDTO>();
            deleteMap.ReverseMap();

        }
    }
}
