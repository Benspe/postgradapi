﻿using AutoMapper;
using PostGradAPI.DTOs.ProfessorResearchPaperDTOs;
using PostGradAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PostGradAPI.Profiles
{
    public class ProfessorResearchPaperProfile : Profile
    {
        public ProfessorResearchPaperProfile() 
        {
            //ReadMap
            var readMap = CreateMap<ProfessorResearchPaper, ProfessorResearchPaperReadDTO>();
            readMap.ForMember(p => p.Professor, pRead => pRead.MapFrom(prp => prp.Professor.FullName));
            readMap.ForMember(rp => rp.ResearchPaper, rpRead => rpRead.MapFrom(prp => prp.ResearchPaper.Title));
            readMap.ReverseMap();

            //AddMap
            var addMap = CreateMap<ProfessorResearchPaper, ProfessorResearchPaperAddDTO>();
            addMap.ReverseMap();
        }
    }
}
