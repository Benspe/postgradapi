﻿using AutoMapper;
using PostGradAPI.DTOs.ResearchPaperDTOs;
using PostGradAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PostGradAPI.Profiles
{
    public class ResearchPaperProfile : Profile
    {
        public ResearchPaperProfile() 
        {


            var readMap = CreateMap<ResearchPaper, ResearchPaperReadDTO>();
            readMap.ForMember
                (
                rpRead => rpRead.AuthorIDs,
                rp => rp.MapFrom
                (
                    prp => prp.ProfessorResearchPaper.Select(p => p.ProfessorID).ToArray()
                )) ;
            readMap.ReverseMap();
        }
    }
}
