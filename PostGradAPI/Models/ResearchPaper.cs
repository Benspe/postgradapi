﻿using System.Collections.Generic;

namespace PostGradAPI.Models
{
    public class ResearchPaper
    {

        public int ID { get; set; }
        public string Title { get; set; }
        public ICollection<ProfessorResearchPaper> ProfessorResearchPaper { get; set; }
    }
}