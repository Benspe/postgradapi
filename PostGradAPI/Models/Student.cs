﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading.Tasks;

namespace PostGradAPI.Models
{
    public class Student
    {

        public int ID { get; set; }

        [Required]
        [MaxLength(255)]
        public string FullName { get; set; }
        public string Major { get; set; }
        public Professor Supervisor { get; set; }
        public int SupervisorID { get; set; }

    }
}
