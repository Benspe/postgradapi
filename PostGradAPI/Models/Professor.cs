﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading.Tasks;


namespace PostGradAPI.Models
{
    public class Professor
    {
        [Required]
        public int ID { get; set; }

        [Required]
        [MaxLength(255)]
        public string FullName { get; set; }

        [MaxLength(255)]
        public string Subject { get; set; }


        public ICollection<ProfessorResearchPaper> ProfessorResearchPapers;

    }
}

