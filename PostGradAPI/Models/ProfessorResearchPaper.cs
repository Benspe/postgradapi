﻿using System.ComponentModel.DataAnnotations;

namespace PostGradAPI.Models
{

        public class ProfessorResearchPaper
        {
            [Required]
            public int ProfessorID { get; set; }
            [Required]
            public int ResearchPaperID { get; set; }

        [Required]
        [MaxLength(255)]
            public Professor Professor { get; set; }
        [Required]
        [MaxLength(255)]
            public ResearchPaper ResearchPaper { get; set; }
        }
}