# postgradapi

![banner](prof-stock.jpg)

[![standard-readme compliant](https://img.shields.io/badge/standard--readme-OK-green.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)

PostGradAPI provides an api for a theoretical university. Professor, Student and ResearchPaper are all tables in a database which this API can query.

## Table of Contents

- [Install](#install)
- [Usage](#usage)
- [Maintainers](#maintainers)
- [Contributing](#contributing)
- [License](#license)

## Install

```
```

## Usage

```
```

## Maintainers

[@Bendik Sperrevik](https://gitlab.com/Benspe)

## Contributing

PRs accepted.

Small note: If editing the README, please conform to the [standard-readme](https://github.com/RichardLitt/standard-readme) specification.

## License

WTFPL © 2020 Bendik Sperrevik
